import mlflow
import torch
from torch import nn, utils
from torch.utils.data import ConcatDataset, random_split, SubsetRandomSampler, DataLoader
from torchvision import transforms, models, datasets, io, ops
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import os
from uuid import uuid4
from sklearn.model_selection import KFold
# import torchmetrics
import mlflow
from rich.progress import Progress
from rich.progress import track


from pprint import pprint
import random
import numpy as np
import matplotlib.pyplot as plt

import torchvision.transforms.functional as F
from torchvision.utils import draw_bounding_boxes


from torchvision.utils import make_grid
from torch.optim import Adam

from torchinfo import summary

import torchvision


plt.rcParams['figure.figsize'] = 32, 5

# Hyperparameters
EPOCHS=35
LEARNING_RATE=3e-4
BATCH_SIZE=48
DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def show(imgs, file_name=None):

    if not isinstance(imgs, list):
        imgs = [imgs]

    fig, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    if file_name:
        # plt.savefig(file_name)
        if mlflow.active_run() is not None:
            mlflow.log_figure(fig, file_name)
            mlflow.end_run()

def custom_collate(batched_data):
    imgs, _ = zip(*batched_data)
    t = torch.stack(imgs)
    t = t.to(torch.float32)
    return t, _

# get the dataset
root = r'Face Detection Dataset'


class FaceDataset(utils.data.Dataset):
    def __init__(self, dir_path, phase_dir, transforms=None):
            
        labels_train_dir = os.listdir(os.path.join(dir_path, 'labels2'))

        self.images, self.targets = [], []

        for label_file in track(labels_train_dir, description=f"Loading {phase_dir} dataset"):
            boxes, areas = [],[]

            img_filename = os.path.splitext(os.path.basename(label_file))[0]+'.jpg'
            img_filepath = os.path.join(dir_path, 'images', phase_dir, img_filename)
                
            if not os.path.exists(img_filepath):
                continue

            img = io.read_image(img_filepath)

            if img.size(0) == 1 or img.size(0) == 4:
                # grayscale images will be omitted
                continue
        
            with open(os.path.join(dir_path, 'labels2',  label_file)) as file:
                lines = file.readlines()
            
            for line in lines:
                box = line.strip('\n').split(' ')[2:]
                box = [int(float(b)) for b in box]
                # width, height = img.size(2), img.size(1)
                # box = [box[0], box[1], int(box[2]+width), int(box[3]+height)]
                x_scale = 640/img.size(2)
                y_scale = 640/img.size(1)
            
                box = [int(box[0]*x_scale), int(box[1]*y_scale), int(box[2]*x_scale), int(box[3]*y_scale)]
                
                if box[0]==box[2] or box[1] == box[3]:
                    
                    continue
                
                boxes.append(box)
                areas.append(box[2] * box[3])

            if len(boxes) == 0:
                boxes = torch.zeros((0, 4))
                areas = torch.zeros((0, 4))
                l1 = torch.zeros((0, 4), dtype=torch.int64)
            else:
                
                l1= torch.ones(len(boxes), dtype=torch.int64)
            self.targets.append({
                'labels': l1,
                'boxes': torch.tensor(boxes, dtype=torch.float32),
                'image_id': uuid4(),
                'areas': torch.tensor(areas, dtype=torch.float32)
            })
            self.images.append(os.path.join(dir_path, 'images', phase_dir, img_filename))
        # print(f'images length = {len(self.images)}')
        self.transforms = transforms
            
    def __getitem__(self, idx):
        img = io.read_image(self.images[idx])
        d = self.targets[idx]
        if self.transforms and isinstance(self.transforms, transforms.transforms.Compose):
            return self.transforms(img), d
        return img, d
    
    def __len__(self):
        return len(self.images)

# get the dataset
root = r'Face Detection Dataset'


class WiderFaceDataset(utils.data.Dataset):
    images = []
    targets=[]
    def __init__(self, dir_path, transforms=None):
        with open(os.path.join(dir_path, 'annotations', 'wider_face_train_bbx_gt.txt'), 'r') as file:
            lines = file.readlines()

        idx = 0
        progressbar = Progress()
        wider_train_pb = progressbar.add_task("Loading wider train", total=len(lines))
        with progressbar:
            while idx < len(lines):
                if not lines[idx].strip('\n').endswith('.jpg'):
                    idx+=1
                    continue
                
                boxes, areas = [],[]
                img_filename = lines[idx].strip('\n')
                img_filepath = os.path.join(dir_path, 'images', img_filename)
                
                if not os.path.exists(img_filepath):
                    continue

                img = io.read_image(img_filepath)
                
                num_boxes = int(lines[idx+1].strip('\n'))
                
                for i in range(1, num_boxes+1):
                    bb = lines[idx+1+i].strip('\n').split()
                    box = list(map(int, bb[:4]))

                    x_scale = 640/img.size(2)
                    y_scale = 640/img.size(1)
                        
                    box = [int(box[0]*x_scale), int(box[1]*y_scale), int((box[0] + box[2])*x_scale), int((box[1]+box[3])*y_scale)]
                    if box[0]==box[2] or box[1] == box[3]:
                        continue
                    
                    boxes.append(box)
                    areas.append(box[2] * box[3])
            
                if len(boxes) == 0:
                    boxes = torch.zeros((0, 4))
                    areas = torch.zeros((0, 4))
                    l1 = torch.zeros((0, 4), dtype=torch.int64)
                else:
                    
                    l1= torch.ones(len(boxes), dtype=torch.int64)
                self.targets.append({
                    'labels': l1,
                    'boxes': ops.box_convert(torch.tensor(boxes, dtype=torch.float32), 'xywh','xyxy'),
                    'image_id': uuid4(),
                    'areas': torch.tensor(areas, dtype=torch.float32)
                })
                self.images.append(img_filepath)
                idx += num_boxes + 2
        
                progressbar.update(wider_train_pb, advance=num_boxes+2)
        
        self.transforms = transforms

    def __getitem__(self, idx):
        img = io.read_image(self.images[idx])
        d = self.targets[idx]
        if self.transforms and isinstance(self.transforms, transforms.transforms.Compose):
            return self.transforms(img), d
        return img, d
    
    def __len__(self):
        return len(self.images)
    


wider_train_dataset = WiderFaceDataset('WIDER_train', transforms=transforms.Compose([
    transforms.Resize((640, 640), antialias=None),
    # transforms.RandomHorizontalFlip(),
    transforms.RandomAutocontrast()
]))

other_train_dataset = FaceDataset(root, phase_dir="train", transforms=transforms.Compose([
    transforms.Resize((640, 640), antialias=None),
    # transforms.RandomHorizontalFlip(),
    transforms.RandomAutocontrast()
    
]))

test_dataset = FaceDataset(root, phase_dir="val", transforms=transforms.Compose([
    transforms.Resize((640, 640), antialias=None),
    # transforms.RandomHorizontalFlip(),
    transforms.RandomAutocontrast()
]))

train_dataset = ConcatDataset([wider_train_dataset, other_train_dataset])
# train_dataset = wider_train_dataset


print(f'Length of train dataset = {len(train_dataset)}')
print(f'Length of wider dataset = {len(wider_train_dataset)}')

class CustomPredictor(torch.nn.Module):
    def __init__(self, n_features, num_classes):
        super().__init__()
        self.num_classes = num_classes
        self.n_features = n_features
        self.create_model()
    
    def create_model(self):
        self.model = nn.Sequential(
            nn.Flatten(), 
            nn.Linear(self.n_features, 1024), 
            nn.ReLU(), 
            nn.Linear(1024, 512), 
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, 512), 
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, 512), 
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, 512), 
            nn.ReLU(),
            nn.BatchNorm1d(512),
            nn.Linear(512, 256), 
            nn.ReLU(),
            nn.BatchNorm1d(256),
        )
        self.cls_score = nn.Linear(256, num_classes)
        self.bbox_pred = nn.Linear(256, num_classes * 4)
# n+2p-k/s+1 = 
    def forward(self, x):
        prefinal_stage = self.model(x)
        scores = self.cls_score(prefinal_stage)
        bbox_deltas = self.bbox_pred(prefinal_stage)
        return scores, bbox_deltas
    
net = models.detection.fasterrcnn_resnet50_fpn()
input_features = net.roi_heads.box_predictor.cls_score.in_features
num_classes=2

# predictor = FastRCNNPredictor(in_channels=input_features, num_classes=num_classes)
# for p in net.parameters():
    # p.requires_grad = False
print(input_features)
predictor = CustomPredictor(n_features=input_features, num_classes=num_classes)
net.roi_heads.box_predictor = predictor


params = [p for p in net.parameters() if p.requires_grad]
print()
print(f'Total trainable params = {sum([p.numel() for p in params])}')
optimizer = Adam(params=params, lr=LEARNING_RATE)


k = torch.randn((3, 1240, 640))
print(summary(net, (3, 3, 1240, 640)))


def mean_average_precision(image_level_detections, true_boxes, iou_threshold):
    '''
    MAP calculation for single class object detection
    '''
    detections = []
    TP = torch.empty(0)
    FP = torch.empty(0)
    # Loop over every image
    box_counter = 0
    
    for idx in range(len(image_level_detections)):    
        if true_boxes[idx]['boxes'].size(0) == 0:
            # no true detections exist
            continue
        # Creaet IoU matrix of (n x m) where 
        # n=number of predicted boxes in current image
        # m= number of actaul annotated boxes in current image
        box_counter += len(image_level_detections[idx]['boxes'])

        iou_matrix = torchvision.ops.box_iou(image_level_detections[idx]['boxes'], true_boxes[idx]['boxes'])
        
        # Filter the places where IoU is less than given threshold
        iou_matrix = torch.where(iou_matrix > iou_threshold, iou_matrix, 0)
        
        # Repeat the maximum element along each row in IoU matrix
        # For example, 
        # input =  [[2, 3, 4], 
        #           [1, 2, 3],
        #           [88, 9, 0]]
        # output = [[ 4,  4,  4],
        #           [ 3,  3,  3],
        #           [88, 88, 88]]
        
        temp = iou_matrix.max(dim=1).values.unsqueeze(1).repeat(1, iou_matrix.size(1))
        # mark only max along each row as 1, rest all as 0
        best_iou  = torch.where((iou_matrix==temp) & (iou_matrix!=0), 1, 0)
        true_positives = best_iou.sum(dim=1)
        image_level_detections[idx]['true_positives'] = true_positives
    
        item = image_level_detections[idx]
        
        packed_item = zip(item['boxes'], item['scores'], item['true_positives'])
        # For every detection, this will become now a tuple of 3 items -> (box, score, true_positive?)
        detections.extend(packed_item)
    try:
        sorted_boxes = sorted(detections, key=lambda x: x[1], reverse=True)
    except ValueError:
        print("Detections after value error")
        print(detections)
    boxes, scores, true_positives = zip(*detections)

    true_positives = torch.tensor(true_positives)
    false_positives = (~true_positives.bool()).int()  # since only a single class object detection problem

    assert len(true_positives) == box_counter
    assert len(false_positives) == box_counter
    
    return  true_positives, false_positives

def plot_prec_recall_curve(recalls, precisions):
    i=1
    plt.figure(figsize=(32, 10))
    for threshold in np.arange(0.5, 0.95, 0.05):
        threshold = round(threshold, 2)
        plt.subplot(5, 2, i)
        plt.plot(recalls[threshold].cpu(), precisions[threshold].cpu())
        plt.xlabel('Recalls')
        plt.ylabel('Precision')
        plt.title(f'Precision vs Recall curve for threshold {threshold}')
        plt.show()
        i+=1

experiment = mlflow.get_experiment_by_name("Face detection with WIDER")
if not experiment:
    # cannot find experiment by name
    # so create it
    experiment_id = mlflow.create_experiment(
        "Face detection with WIDER",
        tags={"version": "v1", "priority": "P1"},
    )
    experiment = mlflow.get_experiment(experiment_id)
print(f"Name: {experiment.name}")
print(f"Experiment_id: {experiment.experiment_id}")
print(f"Artifact Location: {experiment.artifact_location}")
print(f"Tags: {experiment.tags}")
print(f"Lifecycle_stage: {experiment.lifecycle_stage}")
print(f"Creation timestamp: {experiment.creation_time}")


def train_one_epoch(model, data_loader, progressbar):
    ### Training the model
    total_train_loss = total_truths = 0

    model.train()
    model.to(DEVICE)
    for imgs, targets in data_loader:
        new_targets = []
        for t in targets:
            # new_targets.append({k:v for k, v in t.items()})
            new_targets.append({k:v.to(DEVICE)  if isinstance(v, torch.Tensor) else v for k, v in t.items()})
        imgs = imgs.to(DEVICE)
        losses_dict = model(imgs, new_targets)
        loss_train = sum([loss for loss in losses_dict.values()])
        optimizer.zero_grad()
        loss_train.backward()
        optimizer.step()
        
        total_train_loss += loss_train

        progressbar.update(train_task_id, advance=1)
    
    mlflow.log_metric("loss", total_train_loss, epoch+1)
    print(f'Total Training loss for epoch {epoch+1}: {total_train_loss}\n')
    # print(f'Memory usage = {torch.cuda.mem_get_info()}')
    torch.cuda.empty_cache()
    return total_train_loss



# model = torch.nn.DataParallel(net, device_ids=[0, 1])
model = net
start_epoch = 0
if os.path.exists('model.pt'):
    checkpoint = torch.load('model.pt')
    model.load_state_dict(checkpoint['model_state'])
    optimizer.load_state_dict(checkpoint['optimizer_state'])
    start_epoch = checkpoint['epoch']+1
    total_train_loss = checkpoint['loss']
    start_fold = checkpoint['fold']
    print(f'Found trained model. Starting fold at fold {start_fold}')
    print(f"Found trained model. Starting training at epoch {start_epoch}")

mlflow.start_run(experiment_id=experiment.experiment_id)

NUM_FOLDS = 5
THRESHOLD_RANGE = np.arange(0.5, 0.95, 0.05)

folds = KFold(n_splits=NUM_FOLDS, shuffle=True)
dataset = ConcatDataset([train_dataset, test_dataset])
# dataset = torch.utils.data.Subset(train_dataset, range(len(dataset) // 16))
generator2 = torch.Generator().manual_seed(42)
train_dataset, test_dataset = random_split(dataset=dataset, lengths=[0.85, 0.15], generator=generator2)

EPOCHS=15
try:
    with Progress() as progressbar:

        fold_task_id = progressbar.add_task(f"[green]Processing fold", total=NUM_FOLDS)
        epoch_task_id = progressbar.add_task("Running epoch", total=EPOCHS)
        
        for fold_num, (train_indices, val_indices) in enumerate(folds.split(train_dataset)):

            # print(f"Processing fold {fold_num}")
            progressbar.update(fold_task_id, description=f"[green]Processing fold {fold_num+1}")
            train_subsampler = utils.data.SubsetRandomSampler(train_indices)
            val_subsampler = utils.data.SubsetRandomSampler(val_indices)

            train_dataloader = utils.data.DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, sampler=train_subsampler, collate_fn=custom_collate)
            val_dataloader = utils.data.DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, sampler=val_subsampler, collate_fn=custom_collate)
            
            # print(f"Length of train dataloader = {len(train_dataloader) * BATCH_SIZE}")
            # print(f"Length of val dataloader = {len(val_dataloader) * BATCH_SIZE}")
            
            
            for epoch in range(start_epoch, EPOCHS):
                ### Training the model
                progressbar.update(epoch_task_id, description=f"Running epoch {epoch+1}")
                train_task_id = progressbar.add_task("Processing training data...", total=len(train_dataloader))
                
                total_train_loss = total_truths = 0
                
                train_one_epoch(model, train_dataloader, progressbar)
                
                progressbar.remove_task(train_task_id)
                
                # Evaluating model on validation set
                model.eval()

                with torch.no_grad():
                    
                    TP, FP, precisions, recalls,NMS_outputs = {}, {}, {}, {}, {}
                    for threshold in THRESHOLD_RANGE:
                        threshold = round(threshold, 2)

                        precisions[threshold] = torch.empty(0, dtype=torch.float16).to(DEVICE)
                        recalls[threshold] = torch.empty(0, dtype=torch.float16).to(DEVICE)

                        TP[threshold] = torch.empty(0, dtype=torch.float16).to(DEVICE)
                        FP[threshold] = torch.empty(0, dtype=torch.float16).to(DEVICE)

                        NMS_outputs[threshold] = []
                    all_targets=[]

                    val_task_id = progressbar.add_task("Processing validation data...", total=len(val_dataloader))
                    
                    for idx, (imgs, targets) in enumerate(val_dataloader):
                        new_targets = []

                        for t in targets:
                            # new_targets.append({k:v for k, v in t.items()})
                            new_targets.append({k:v.to(DEVICE)  if isinstance(v, torch.Tensor) else v for k, v in t.items()})
                            total_truths += t['boxes'].size(0)

                        imgs = imgs.to(DEVICE)
                        losses_dict = model(imgs)

                        outputs = [{k: v.to(DEVICE) for k, v in t.items()} for t in losses_dict]

                        for threshold in THRESHOLD_RANGE:

                            threshold = round(threshold, 2)
                            for item in outputs:
                                box_to_keep = ops.nms(item['boxes'], item['scores'], threshold)          

                                NMS_outputs[threshold].append({
                                    'boxes': torch.index_select(item['boxes'],0, box_to_keep).to(device=DEVICE, dtype=torch.int32),
                                    'scores': torch.index_select(item['scores'],0, box_to_keep).to(device=DEVICE, dtype=torch.float16),
                                    'labels': torch.index_select(item['labels'],0, box_to_keep).to(device=DEVICE, dtype=torch.int8)
                                })

                        all_targets.extend(new_targets)

                        if (idx+1)%50 == 0:
                            imgs_disp = torch.clone(imgs).to(dtype=torch.uint8)
                            items = random.sample(list(zip(imgs_disp, NMS_outputs[0.5], targets)), min(4, BATCH_SIZE, imgs_disp.size(0)))
                            imgs_with_boxes = []
                            for img, output, real_target in items:
                                imgs_with_boxes.append(draw_bounding_boxes(img, boxes=output['boxes'], width=4, colors="green").to(torch.device('cpu')))
                                imgs_with_boxes.append(draw_bounding_boxes(img, boxes=real_target['boxes'], width=4, colors="red").to(torch.device('cpu')))
                            grid = make_grid(imgs_with_boxes)
                            plt.rcParams["savefig.bbox"] = 'tight'
                            show(grid, file_name=f"figures/{str(epoch+1).zfill(2)}-{idx+1}.jpg")
                            # plt.show()   # to show in jupyter notebook
                        progressbar.update(val_task_id, advance=1)

                    progressbar.remove_task(val_task_id)

                    if fold_num>=3:
                        map_task = progressbar.add_task("[cyan]Mean Average Precision@", total=len(THRESHOLD_RANGE))
                        for threshold in THRESHOLD_RANGE:
                            threshold = round(threshold, 2)
                            progressbar.update(map_task, description=f"[cyan]Mean Average Precision@{threshold}")
                            TP[threshold], FP[threshold] = mean_average_precision(NMS_outputs[threshold], all_targets, threshold)
                            TP[threshold] = torch.cumsum(TP[threshold], dim=0).to(dtype=torch.int32, device=DEVICE)
                            FP[threshold] = torch.cumsum(FP[threshold], dim=0).to(dtype=torch.int32, device=DEVICE)

                            mlflow.log_metric(key=f"True positive {threshold}", value=TP[threshold][-1], step=epoch+1)
                            mlflow.log_metric(key=f"False positive {threshold}", value=FP[threshold][-1], step=epoch+1)

                            
                            print(f"TP[{threshold}] = {TP[threshold][-1]}, FP[{threshold}] = {FP[threshold][-1]}")
                            progressbar.update(map_task, advance=1)

                    for threshold in THRESHOLD_RANGE:
                        threshold = round(threshold, 2)
                        truth_tensor = torch.tensor([total_truths]*TP[threshold].size(0)).to(dtype=torch.float16, device=DEVICE)
                        recalls[threshold]= TP[threshold] / truth_tensor 
                        precisions[threshold]= TP[threshold] / (TP[threshold] + FP[threshold])

                    # plot_prec_recall_curve(recalls, precisions)
                progressbar.update(epoch_task_id, advance=1)
                
                torch.save({
                    'model_state': model.state_dict(),
                    'optimizer_state': optimizer.state_dict(),
                    'epoch': epoch,
                    'loss': total_train_loss,
                    'fold': fold_num
                }, 'model.pt')
                
            start_epoch = 0
            if 'map_task' in locals():
                progressbar.remove_task(map_task)
            progressbar.update(fold_task_id, advance=1)
finally:
    mlflow.end_run()